from pathlib import Path
from unittest.mock import MagicMock

from flywheel_gear_toolkit.context import GearToolkitContext

from fw_gear_ants_buildtemplateparallel.workflow import run_workflow


def test_run_workflow(mocker, tmpdir):
    context = MagicMock(spec=GearToolkitContext)
    context.config = {
        "Input Tags": "one-tag,another-tag",
        "Image Dimension": 3,
        "Max Iterations": "10x20x30",
        "N4 Bias Field Correction": True,
        "Rigid-body Registration": False,
        "Transformation Model": "GR",
        "Similarity Metric": "CC",
    }
    input_path = Path(tmpdir) / "t1.nii.gz"
    input_path.touch()
    input_paths = ["t1.nii.gz"]
    template = Path(tmpdir) / "template.nii.gz"
    template.touch()
    final_template = Path(tmpdir) / "final_template.nii.gz"
    final_template.touch()
    btpe = mocker.patch(
        "fw_gear_ants_buildtemplateparallel.workflow.buildtemplateparallelEnhanced"
    )
    results = MagicMock()
    results.outputs.final_template_file = str(final_template)
    btpe.return_value.run.return_value = results
    output_dir = tmpdir / "output"
    run_workflow(input_paths, tmpdir, context, template, output_dir)

    assert (output_dir / final_template.name).exists()
