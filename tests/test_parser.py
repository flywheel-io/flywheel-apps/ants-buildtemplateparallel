"""Module to test parser.py"""
from unittest.mock import MagicMock

import flywheel
import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_buildtemplateparallel.parser import parse_config


def test_parse_config():
    context = MagicMock(spec=GearToolkitContext)
    context.config = {
        "Input Regex": "my-regex",
        "Input Glob Pattern": "my-glob",
        "Input Tags": "one-tag,another-tag",
        "Target Template": "MNI152_T1_1mm.nii.gz",
    }
    context.get_destination_parent.return_value = flywheel.Project(label="Root")
    regex, glob_pattern, tags, target, destination_parent = parse_config(context)

    assert regex == "my-regex"
    assert glob_pattern == "my-glob"
    assert tags == ["one-tag", "another-tag"]
    assert destination_parent == flywheel.Project(label="Root")


def test_parse_config_raise_if_tag_and_regex_are_undefined():
    context = MagicMock(spec=GearToolkitContext)
    context.config = {
        "Input Regex": "",
        "Input Tags": "",
    }
    with pytest.raises(SystemExit):
        parse_config(context)
