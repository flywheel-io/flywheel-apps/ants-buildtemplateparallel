import flywheel
import pytest

from fw_gear_ants_buildtemplateparallel.utils import FileMatcher


def test_is_file_match_raise_if_tags_not_list():
    with pytest.raises(TypeError):
        FileMatcher(tags="my-tag")


def test_is_file_match_exit_if_regex_is_invalid():
    with pytest.raises(SystemExit):
        FileMatcher(regex_pattern="*.")


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=[]), False),
    ],
)
def test_is_file_match_with_tags(f, res):
    fm = FileMatcher(tags=["my-tag"])
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.dicom.zip", type="dicom", tags=[]), False),
    ],
)
def test_is_file_match_with_regex(f, res):
    fm = FileMatcher(regex_pattern=".*.nii.gz")
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz", type="nifti", tags=["my-tag"]), True),
        (flywheel.FileEntry(name="T1w.dicom.zip", type="dicom", tags=[]), False),
    ],
)
def test_is_file_match_with_regex(f, res):
    fm = FileMatcher(filetype="nifti")
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz"), True),
        (flywheel.FileEntry(name="T1w.dicom.zip"), False),
    ],
)
def test_is_file_match_with_glob_pattern(f, res):
    fm = FileMatcher(glob_pattern="*.nii.gz")
    assert fm.match(f) is res


@pytest.mark.parametrize(
    "f,res",
    [
        (flywheel.FileEntry(name="T1w.nii.gz"), True),
        (flywheel.FileEntry(name="T1w.dicom.zip"), True),
    ],
)
def test_is_file_match_with_glob_pattern_and_regex(f, res):
    fm = FileMatcher(glob_pattern="*.nii.gz", regex_pattern=r".*zip")
    assert fm.match(f) is res
