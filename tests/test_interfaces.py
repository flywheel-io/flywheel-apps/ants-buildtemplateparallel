from fw_gear_ants_buildtemplateparallel.interfaces import buildtemplateparallelEnhanced


def test_buildtemplateparallelEnhanced():
    interface = buildtemplateparallelEnhanced()
    assert hasattr(interface.inputs, "template")
