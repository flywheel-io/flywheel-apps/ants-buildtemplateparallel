# Release notes

## 0.1.1_2.3.5

* Add support for glob pattern to define input files.

## 0.1.0_2.3.5

* Initial release.