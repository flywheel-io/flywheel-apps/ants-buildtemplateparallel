#!/usr/bin/env python
"""The run script"""
import logging
import sys
from pathlib import Path
from tempfile import TemporaryDirectory

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_ants_buildtemplateparallel import TEMPLATES_DIR
from fw_gear_ants_buildtemplateparallel.parser import parse_config
from fw_gear_ants_buildtemplateparallel.utils import download_files, find_matching_files
from fw_gear_ants_buildtemplateparallel.workflow import run_workflow

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""
    regex, glob_pattern, tags, target, destination_parent = parse_config(context)

    # download files inputs
    input_files = find_matching_files(
        destination_parent,
        tags=tags,
        regex=regex,
        glob_pattern=glob_pattern,
        filetype="nifti",
    )
    data_dir = Path(TemporaryDirectory().name)
    input_paths = download_files(input_files, dest_dir=data_dir)
    in_files = [p.name for p in input_paths]

    # get target template
    if target == "None":
        target = None
    else:
        target = TEMPLATES_DIR / target
        if not target.exists():
            raise ValueError(f"Not found: {target}")

    try:
        run_workflow(in_files, data_dir, context, target, context.output_dir)
    except Exception as exc:
        log.exception(f"Workflow raises following exception at runtime:\n {exc}")
        raise exc


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
