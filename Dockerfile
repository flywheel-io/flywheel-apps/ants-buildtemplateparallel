FROM flywheel/fw-gear-ants-base:2.3.5

ENV FLYWHEEL="/flywheel/v0"

RUN pip install "poetry==1.1.2"

WORKDIR ${FLYWHEEL}
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev

COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_ants_buildtemplateparallel $FLYWHEEL/fw_gear_ants_buildtemplateparallel
RUN poetry install --no-dev

RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
